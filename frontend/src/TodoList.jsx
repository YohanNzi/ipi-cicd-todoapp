// src/Items.jsx
import { useState, useEffect } from 'react';
import { addItem, deleteItem, readItems, updateItem } from './api';
import TodoItem from './components/TodoItem';
import AddTodo from './components/AddTodo';

import { TodoListError } from './components/TodoListError';
import DismissibleAlert from './components/DismisibleAlert';
import './TodoList.css';

const TodoList = () => {
  const [items, setItems] = useState([]);
  const [name, setName] = useState('');
  const [error, setError] = useState(null);
  const [alert, setAlert] = useState(null);

  useEffect(() => {
    console.log('effect');
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const fetchedItems = await readItems();
      setItems(fetchedItems);
    } catch (error) {
      console.error('Error fetching items:', error);
      setError(error);
      setAlert({ message: error.message, status: 'error' });
    }
  };

  const handleAddItem = async (e) => {
    e.preventDefault();
    try {
      const createdItem = await addItem(name);
      setName('');
      setItems([...items, createdItem]);
      setAlert({ message: 'Item added', status: 'success' });
    } catch (error) {
      console.error('Error adding item:', error);
      setAlert({ message: error.message, status: 'error' });
    }
  };

  const handleDeleteItem = async (id) => {
    try {
      await deleteItem(id);
      setItems(items.filter((item) => item.id !== id));
      setAlert({ message: 'Item deleted', status: 'success' });
    } catch (error) {
      console.error('Error deleting item:', error);
      setAlert({ message: error.message, status: 'error' });
    }
  };

  const handleUpdateItem = async (id, payload) => {
    try {
      const updatedItem = await updateItem(id, payload);
      setItems((prev) =>
        prev.map((item) => (item.id === id ? updatedItem : item))
      );
      setAlert({ message: 'Item updated', status: 'success' });
    } catch (error) {
      console.error('Error updating item:', error);
      setAlert({ message: error.message, status: 'error' });
    }
  };

  if (error) {
    return <TodoListError message={error.message} />;
  }

  return (
    <div className="TodoList">
      <h2>Tasks</h2>
      <DismissibleAlert alert={alert} setAlert={setAlert} />
      <AddTodo name={name} setName={setName} handleAddItem={handleAddItem} />
      <ul className="items">
        {items.map((item) => (
          <TodoItem
            key={item.id}
            item={item}
            handleDeleteItem={handleDeleteItem}
            handleUpdateItem={handleUpdateItem}
          />
        ))}
      </ul>
    </div>
  );
};

export default TodoList;
