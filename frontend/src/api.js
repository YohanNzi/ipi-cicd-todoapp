import axios from 'axios';
import { serverUrl } from './settings';

const api = axios.create({
  baseURL: `${serverUrl}/api`,
});

export async function readServerMessage() {
  const response = await api.get('/message');
  return response.data;
}

export async function readItems() {
  const response = await api.get('/items');
  return response.data;
}

export async function addItem(name) {
  const response = await api.post('/items', { name });
  return response.data;
}

export async function deleteItem(id) {
  const response = await api.delete(`/items/${id}`);
  return response.data;
}

export async function updateItem(id, payload) {
  const response = await api.put(`/items/${id}`, payload);
  return response.data;
}